import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../common/services/authentication.service';
import {SessionStorageService} from 'ngx-webstorage';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any = <any>{};

  constructor(public _authService : AuthenticationService,
              public _locker: SessionStorageService,
              public _router: Router) { }

  ngOnInit() {
  }

  onSubmit(event: Event) { // Recibimos el evento Submit
    event.preventDefault(); // Se cancela el evento para que no se recargue la página

    this._authService.logIn(this.user.username, this.user.password).subscribe(  // Nos suscribimos a la respuesta del logIn
        (data) => { // SI es exito se:
          this._authService.user = data;  // Guarda el usuario
          this._authService.hasSession = true; // Se coloca la sesión en True
          this._locker.store('user', data); // Se almacna en sesionStorage o LocalStorage el usuario completo
          this._router.navigate(['/home']); // y hacemos un Redirect To /home
        },
        err => {
          console.error(err); // Si hay un error se despliega en la consola y colocamos
          this._authService.hasSession = false; // Sesión en False
        }
    );
  }
}
